package com.hendisantika.retrofitspringboot.controller;

import com.google.gson.Gson;
import com.hendisantika.retrofitspringboot.client.CountryClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * Project : retrofit-springboot
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/12/19
 * Time: 17.46
 */
@Slf4j
@RestController
public class CountryController {

    @Autowired
    CountryClient resp;

    @GetMapping(value = "/allCountry")
    public ResponseEntity allCountry() throws IOException {
        Gson gson = new Gson();
        String jsonInString;
        jsonInString = gson.toJson(resp.listAll().execute().body());

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(jsonInString);
    }

    @GetMapping(value = "/allCountryByName/{name}")
    public ResponseEntity allCountryByName(@PathVariable("name") String name) throws IOException {
        Gson gson = new Gson();
        String jsonInString;
        jsonInString = gson.toJson(resp.listAllCountryName(name).execute().body());

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(jsonInString);
    }

    @GetMapping(value = "/allCountryCustom")
    public ResponseEntity allCountryCustom() throws IOException {
        Gson gson = new Gson();
        String jsonInString;
        jsonInString = gson.toJson(resp.listAllCustom().execute().body());

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(jsonInString);
    }
}
