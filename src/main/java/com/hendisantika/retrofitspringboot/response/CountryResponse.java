package com.hendisantika.retrofitspringboot.response;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by IntelliJ IDEA.
 * Project : retrofit-springboot
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/12/19
 * Time: 17.42
 */
@Getter
@Setter
public class CountryResponse {
    public String name;
    public String capital;
    public String region;
    public String subregion;
    public String nativeName;
}
