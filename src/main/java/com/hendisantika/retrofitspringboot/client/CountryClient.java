package com.hendisantika.retrofitspringboot.client;

import com.hendisantika.retrofitspringboot.response.CountryResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : retrofit-springboot
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 15/12/19
 * Time: 17.41
 */
public interface CountryClient {
    @GET("v2")
    Call<List> listAll();

    @GET("v2")
    Call<List<CountryResponse>> listAllCustom();

    @GET("v2/name/{name}")
    Call<List> listAllCountryName(@Path("name") String name);
}