package com.hendisantika.retrofitspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetrofitSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(RetrofitSpringbootApplication.class, args);
	}

}
