# Implementation Retrofit on Spring Boot
## Project Description
This Project use for retrieve API content
## Build and Run Application 
>- mvn clean install spring-boot:run
### Open Browser and Copy this url :
 GET | http://localhost:8080/allCountry
 
 GET | http://localhost:8080/allCountryCustom
 
 GET | http://localhost:8080/allCountryByName/Indonesia
